// Sniffles.cpp : Defines the entry point for the console application.
//
#include "sniffles.h"

const unsigned char g_iceKey[] = { 0x43, 0x53, 0x47, 0x4F, 0x68, 0x35, 0x00, 0x00, 0x5A, 0x0D, 0x00, 0x00, 0x56, 0x03, 0x00, 0x00, };

int svc_PacketEntitiesTotal = 0;

void ReadPacket(unsigned char* packetData, int size)
{	
	if (size < 8)
		return;

	unsigned char data[NET_MAX_PAYLOAD];
	CBitRead buf(data, NET_MAX_PAYLOAD);
	memcpy((void*)buf.GetBasePointer(), packetData, size);
	buf.Seek(0);

	int32 nSeqNrIn = buf.ReadSBitLong(32); // SeqNrIn
	int32 nSeqNrOut = buf.ReadSBitLong(32); // SeqNrOut
	
	byte flags = buf.ReadByte();
	short checksum = buf.ReadShort();
	byte reliableState = buf.ReadByte();

	if (!flags || flags >= 0xE1u)
	{
		int i = 0;
		while (buf.GetNumBytesRead() < size)
		{
			int Cmd = buf.ReadVarInt32();
			int Size = buf.ReadVarInt32();


			void* parseBuffer = (void*)((int)buf.GetBasePointer() + buf.GetNumBytesRead());
			size_t bufferSize = Size;

			switch (Cmd)
			{
				//case net_Tick:
				//{
				//	CNETMsg_Tick msg;
				//	if (msg.ParseFromArray(parseBuffer, bufferSize))
				//		MsgPrintf(msg, bufferSize, "%s", msg.DebugString().c_str());
				//}
				//break;

				//case net_SignonState:
				//{
				//	CNETMsg_SignonState msg;
				//	if (msg.ParseFromArray(parseBuffer, bufferSize))
				//		MsgPrintf(msg, bufferSize, "%s", msg.DebugString().c_str());
				//}
				//break;

				//case svc_ServerInfo:
				//{
				//	CSVCMsg_ServerInfo msg;
				//	if (msg.ParseFromArray(parseBuffer, bufferSize))
				//		MsgPrintf(msg, bufferSize, "%s", msg.DebugString().c_str());
				//}
				//break;

				case svc_PacketEntities:
				{
					svc_PacketEntitiesTotal++;

					outf("total: %d", svc_PacketEntitiesTotal);

					CSVCMsg_PacketEntities msg;
					if (msg.ParseFromArray(parseBuffer, bufferSize))
						MsgPrintf(msg, bufferSize, "%s", msg.DebugString().c_str());

				}
				break;

				default:
					break;

				i++;
			}

			buf.SeekRelative(Size * 8);
		}
	}
}

void packet_handler(uint8* pData, size_t size)
{
	IceKey ice(2);
	ice.set(g_iceKey);
		
	uint8* pDataOut = (uint8*)malloc(size);
		
	int32 blockSize = ice.blockSize();
		
	uint8* p1 = pData;
	uint8* p2 = pDataOut;
		
	// encrypt data in 8 byte blocks
	int32 bytesLeft = size;
		
	while (bytesLeft >= blockSize)
	{
		ice.decrypt(p1, p2);
		
		bytesLeft -= blockSize;
		p1 += blockSize;
		p2 += blockSize;
	}
		
	//The end chunk doesn't get an encryption. it sux.
	memcpy(p2, p1, bytesLeft);

	unsigned char deltaOffset = *(unsigned char*)pDataOut;
	if (deltaOffset > 0 && (uint32)deltaOffset + 5 < size)
	{
		uint32 dataFinalSize = _byteswap_ulong(*(uint32*)&pDataOut[deltaOffset + 1]);
		if (dataFinalSize + deltaOffset + 5 == size)
		{
			uint8* packetData = (uint8*)malloc(dataFinalSize);
			memcpy(packetData, &pDataOut[deltaOffset + 5], dataFinalSize);
			free(pDataOut);
			if (packetData)
			{
				ReadPacket(packetData, dataFinalSize);
				free(packetData);
			}
		}
	}			
}


int _tmain(int argc, _TCHAR* argv[])
{
	char search_path[] = "D:/dev/projectX/sniffles/Debug/sniff/*.bin";
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path, &fd);

	search_path[37] = '\0';
	do 
	{
		// read all (real) files in current folder
		// , delete '!' read other 2 default folder . and ..
		if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			char* filename = (char*)malloc(strlen(search_path) + strlen(fd.cFileName) + 1);

			filename[0] = '\0';   // ensures the memory is an empty string
			strcat(filename, search_path);
			strcat(filename, fd.cFileName);

			struct stat sb;
			stat(filename, &sb);
			char* buff = (char*)malloc(sb.st_size);

			FILE *fp = fopen(filename, "r");
			fgets(buff, sb.st_size, (FILE*)fp);
			fclose(fp);

			//outf("filename: %s", filename);

			packet_handler((uint8*)buff, sb.st_size);

			free(buff);
		}
	} 
	while (::FindNextFile(hFind, &fd));

	::FindClose(hFind);

	return 0;
}

